Quick setup guide for boxbackup system
--------------------------------------

NOTE: The debian package should handle most of the configuration
for you via debconf.

However this is a quick guide if you prefer to do this by
yourself.

If you want to use debconf to configure Boxbackup, do NOT follow
those explanations. Jump directly to the section on Managing certificates

Boxbackup-server configuration
------------------------------

You need to create the server configuration files contained in
/etc/boxbackup.

For this you must first use the raidfile-config script.

raidfile-config /etc/boxbackup 2048 /raid/0.0 /raid/0.1 /raid/0.2

where:
- /etc/boxbackup is the location of the configuration files (don't
 change that as several scripts use that by default)
- 2048 is the block size of the RAID system, this should be set to
 the block size of the underlying filesystem
- the three following path names are the location of the 3 RAID partitions
 used by boxbackup to store the backup. They should be on 3 different
 physical drive. You can disable the use of userland RAID by specifying
 only one path name.

You should now have a file /etc/boxbackup/raidfile.conf that you can
customize to add another set of disc.

Now run the bbstored-config script:

bbstored-config /etc/boxbackup serverhostname bbstored

where:
- /etc/boxbackup is the location of the configuration files (don't 
  change that as several scripts use that by default).
- serverhostname is the fqdn name of the server you are installing on,
  this is used to determine on which interface the daemon will listen on. 
- bbstored is the user the server will run under, this user is automatically
  created by the Debian package.

Now you have to manage your certificate. See below for this.

To manage the client accounts use the bbstoreaccounts utility.
To add an account:
bbstoreaccounts create ACCOUNT_NUMBER DISC_SET SOFT_QUOTA HARD_QUOTA

where:
- ACCOUNT_NUMBER is the account number to create, a 8 digits hexadecimal number.
- DISC_SET is a disc set number defined in /etc/boxbackup/raidfile.conf where the
  files for that account will go into.
- SOFT_QUOTA is the soft storage quota size, the client will avoid to upload files
  when reaching that limit
- HARD_QUOTA is the hard storage quota size, the server will not store files when 
  reaching that limit.

An example invocation:
bbstoreaccounts create 1EF235CA 0 1024M 1250M
(suffixes M, G and B are accepted for quota size meaning respectively Megabytes,
Gigabytes and Blocks)

Boxbackup-client configuration
------------------------------

You need to create the client configuration files contained in
/etc/boxbackup.

For this you must use the bbackupd-config script.

bbackupd-config /etc/boxbackup lazy ACCOUNT_NUMBER SERVER_NAME /var/lib/bbackupd BACKUP_DIR [[BACKUP_DIR]...]

where:
- /etc/boxbackup is the location of the configuration files (don't
  change that as several scripts use that by default).
- lazy: backup mode, could be lazy (continuous scan of filesystem) or
  snapshot (backup launch by a cron script, see /etc/cron.d/boxbackup-client)
- ACCOUT_NUMBER: your account number provided by the backup server administrator
- SERVER_NAME is the fqdn name of the server you will connect to.
- /var/lib/bbackupd: location of working directory (don't change)
- BACKUP_DIR: a list of directories to backup (they must not contain another
  mounted filesystem)


Managing certificates
---------------------

For this you need to use the bbstored-certs script contained in the
boxbackup-server package.

To initialise your CA (creates a "ca" directory with private key and certificate in it) launch:
bbstored-certs ca init

To sign a server certificate:
bbstored-certs ca sign-server server-csr.pem

To sign a client certificate:
bbstored-certs ca sign clientaccount-csr.pem

You will find a more detailed documentation on the boxbackup Web site:
http://www.boxbackup.org


"Upstream" tarball
------------------

The "upstream" tarball is produced using git-archive from the upstream
git branch at github:

https://github.com/boxbackup/boxbackup

 -- Reinhard Tartler <siretart@tauware.de>, Sat, 17 Jun 2017 17:57:00 -0400
